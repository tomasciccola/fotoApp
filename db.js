const mongo = require('mongodb')
const MongoClient = mongo.MongoClient
const Grid = require('gridfs')
const crypto = require('crypto')
const resize = require('resize-optimize-images')
const sharp = require('sharp')

module.exports = (cb) => {
  return connect(cb)
}

function connect (cb) {
  const MONGO_PASS = process.env.NODE_ENV === 'prod' ? process.env.MONGO_PASS : ''
  const config = process.env.NODE_ENV === 'prod' ? JSON.parse(process.env.APP_CONFIG) : ''
  const MONGO_URL = process.env.NODE_ENV === 'prod'
    ? `mongodb://${config.mongo.user}:${encodeURIComponent(MONGO_PASS)}@${config.mongo.hostString}`
    : 'mongodb://localhost:27017'

  // const DB_NAME = 'fotoApp'
  const DB_NAME = 'b8997189d6c897fa15e822992d3d8ea3'
  const COLL_NAME = 'usuarios'

  var db, usuarios, fs

  const getNewUsrId = () => crypto.randomBytes(12).toString('hex')

  MongoClient.connect(MONGO_URL, (err, client) => {
    if (err) return cb(err)
    db = client.db(DB_NAME)
    usuarios = db.collection(COLL_NAME)
    fs = Grid(db, mongo)

    return cb(null, {
      getUsers,
      addUser,
      deleteUsers,
      purgeDb,
      getImg,
      testUser
    })
  })

  function getUsers (n, cb) {
    usuarios.find().sort({ _id: -1 }).limit(n).toArray((err, result) => {
      if (err) return cb(err)
      return cb(null, result)
    })
  }

  function addUser (fields, files, cb) {
    var filename = ''

    if (files['pic'].size !== 0) {
      filename = `${getNewUsrId()}.jpg`
      fields['path'] = filename

      resizeImg(files['pic'].path, (err, path) => {
        if (err) return cb(err)
        fs.fromFile({ filename: filename }, path, (err, file) => {
          if (err) return cb(err)
          saveRecord(path)
        })
      })
    } else {
      filename = ''
      fields['path'] = filename
      saveRecord()
    }

    function resizeImg (path, cb) {
      var outFile = `${path.split('.')[0]}_out.jpg`
      sharp(path)
        .metadata((err, meta) => {
          if (err) return cb(err)
          var ar = meta.width / meta.height
          var w = 640
          var h = w / ar
          sharp(path)
            .rotate()
            .resize(w, Math.floor(h), { fit: 'inside' })
            .toFile(outFile, (err, res) => {
              if (err) return cb(err)
              cb(null, outFile)
            })
        })
    }

    function saveRecord (path) {
      usuarios.insertOne(fields, (err) => {
        if (err) return cb(err)
        return cb(null, fields)
      })
    }
  }

  function deleteUsers (n, cb) {
    usuarios.find().sort({ _id: 1 }).limit(n).toArray((err, arr) => {
      if (err) return cb(err)
      arr.forEach((elem, idx) => {
        fs.files.remove({ filename: elem.path }, (err, result) => {
          if (err) return cb(err)
          usuarios.remove({ _id: elem._id }, (err, result2) => {
            if (err) return cb(err)
            if (arr.length - 1 === idx) {
              return cb(null)
            }
          })
        })
      })
    })
  }

  function purgeDb (cb) {
    return deleteUsers(0, (err) => {
      if (err) return cb(err)
      return cb(null)
    })
  }

  function getImg (path, cb) {
    fs.readFile({ filename: path }, (err, img) => {
      if (err) return cb(err)
      return cb(null, img)
    })
  }

  function testUser (fields, cb) {
    var filename = `${getNewUsrId()}.jpg`

    fields['path'] = filename

    fs.fromFile({ filename: filename }, 'test.jpg', (err, file) => {
      if (err) return cb(err)
      usuarios.insertOne(fields, (err) => {
        if (err) return cb(err)
        return cb(null, fields)
      })
    })
  }
}
