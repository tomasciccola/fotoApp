const html = require('choo/html')
const css = require('sheetify')

// Components
const ImgInput = require('./imgInput.js')
const NameInput = require('./nameInput.js')

const imgInput = new ImgInput()
const nameInput = new NameInput()

const style = css`
  :host {
    grid-area:main;
    background:url(assets/img/fondoMain.png);
    background-size:250%;
    background-position:top -25px left -120px;
    background-repeat:no-repeat;
    display:grid;
    grid-template-rows: 0.25fr 0.5fr 0.25fr;
    grid-template-columns: 0.33fr 0.66fr;
    grid-template-areas:
    "nombre nombre"
    "msg msg"
    "btnFoto btnShare";
    padding:0 4em;
    margin-top:-10%;
  }

  :host div.nombreField{
    grid-area:nombre;
  }

  :host div.nombreField input{
    padding:0.5em;
  }

  :host div.msgField{
    grid-area:msg;
    height:100%;
  }

  :host #wordCounter{
    text-align:right;
    font-size:2.0em;
    font-family:AvenirBlack;
    color:white;
    margin-top:4px;
  }

  :host div.btnFoto{
    grid-area:btnFoto;
    margin-top:10px
  }

  :host .btnFoto{
      position:relative;
      overflow:hidden;
      display:inline-block;
      cursor:pointer;
    }

  :host .btnFoto input[type=file] {
    font-size: 100px;
    position:absolute;
    left:0;
    top:0;
    opacity:0;
  }
  :host .btnFoto button{
    background:url(assets/img/cam.png) no-repeat 0 0 transparent;
    background-size:contain;
    width:100%;
    height:100%;
    border:none;
  }


  :host input.btnShare{
    grid-area:btnShare;
    background:url(assets/img/compartir.png) no-repeat 0 0 transparent;
    background-size:contain;
    cursor:pointer;
    border:none;
    margin-top:15%;
  }

  :host input.btnShare:active{
    transform:translateY(8px);
    outline:none;
    border:0;
    -webkit-tap-highlight-color: transparent;
  }
  :host input.btnShare:focus{
    outline:none;
    border:0;
    -webkit-tap-highlight-color: transparent;
  }

  :host label{
    text-transform:uppercase;
    font-size:3.0em;
    color:white;
    text-shadow:2px 2px 4px rgba(0,0,0,.4);
    font-family:AvenirBlack;
  }

  :host input:not([type='submit']) {
    font-family:AvenirBook;
    font-weight:bold;
    font-size:2.0em;
    margin-top:5%;
    width:100%;
    box-shadow:2px 2px 4px rgba(0,0,0,.4);
    outline:none;
    border:none;
    resize:none;
    overflow:hidden;
  }

  :host textarea{
    margin-top:8px;
    padding:0.5em;
    font-family:AvenirBook;
    font-weight:bold;
    font-size:2.0em;
    box-shadow:2px 2px 4px rgba(0,0,0,.4);
    width:100%;
    height:80%;
    max-height:80%;
    border:none;
    resize:none;
    outline:none;
    overflow:hidden;
  }
`

module.exports = (state, emit) => {
  return html`
    <form class=${style} onsubmit=${onsubmit}>
      <div class="nombreField">
        <label for='nombre'> nombre </label><br>
        ${nameInput.render(state.sendingStatus)}
      </div>

      <div class="msgField">
        <label for='mensaje'> ${state.consigna} </label><br>
        <textarea oninput=${textInput} maxlength=${state.msgLimit} id='mensaje' name='mensaje' type='text'> ${state.msg}</textarea>
        <p id="wordCounter">${state.msgLenDisplay}/${state.msgLimit}</p>
      </div>
      ${imgInput.render(state.sendingStatus)}
      <input style=${setShareButtonStyle()} class="btnShare" name='submit' type='submit' value=''>
    </form>
  `
  function setShareButtonStyle () {
    if (state.canShare) {
      return ''
    } else {
      return 'filter:brightness(0.5);'
    }
  }
  function textInput (e) {
    e.preventDefault()
    emit('newChar', e.target.value)
  }

  function onsubmit (e) {
    e.preventDefault()
    if (state.canShare) {
      var form = e.currentTarget
      var body = new FormData(form)
      emit('newFoto', body)
    }
  }
}
