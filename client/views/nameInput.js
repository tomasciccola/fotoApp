const Nanocomponent = require('nanocomponent')
const html = require('choo/html')

module.exports = class NameInput extends Nanocomponent {
  constructor (style) {
    super()
    this.style = style
  }

  createElement (stat) {
    this.stat = stat
    return html`
        <input id='nombre' name='nombre' type='text'>
    `
  }

  update (stat) {
    return stat === 'sent'
  }
}
