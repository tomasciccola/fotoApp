const Nanocomponent = require('nanocomponent')
const html = require('choo/html')

module.exports = class ImgInput extends Nanocomponent {
  constructor (style) {
    super()
    this.style = style
  }

  createElement (stat) {
    this.stat = stat
    return html`
      <div class="btnFoto">
        <button></button>
        <input type="file" name="pic" id="pic" accept="image/gif, image/jpeg" />
      </div>
    `
  }

  update (stat) {
    return (stat === 'sent')
  }
}
