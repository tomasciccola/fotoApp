const choo = require('choo')
const devtools = require('choo-devtools')
const html = require('choo/html')
const xhr = require('xhr')

// CSS
const css = require('sheetify')
css('tachyons')

const style = css`
  :host{
    background-color:#1B9AAA;
    display:grid;
    grid-template-rows: 0.25fr 0.65fr 0.1fr;
    grid-template-areas:
    "header"
    "main"
    "footer";
    height:100vh;
    width:auto;
    font-size:10px;
  }

  @font-face{
    font-family:AvenirBlack;
    src:url('assets/fonts/AvenirLTStd-Black.otf');
  }

  @font-face{
    font-family:AvenirBook;
    src:url('assets/fonts/AvenirLTStd-Book.otf');
  }

  @media only screen and (min-width:1920px),(min-width:1280px), (min-width:1000px){
    body{
      width:30vw!important;
      margin-left:35vw!important;
    }
  }

  @media only screen and (max-height: 500px) {
    :host header img{
      display:none;
    }
    :host div.btnFoto {
      display:none;
    }

    :host .btnShare{
      display:none;
    }

    :host {
      grid-template-rows: 0.15fr 0.90fr 0.05fr;
    }
  }

  @media only screen and (max-height: 568px){
    body{
      font-size:9px!important;
    }
  }
  

  :host header {
    grid-area:header;
    background-image:url(assets/img/fondoTitle.png);
    background-size:150vw;
    background-position: bottom;
    background-repeat:no-repeat;
  }
  :host header img {
    object-fit:contain;
    height:20vh;
    width:95vw;
  }

  :host footer{
    background:url(assets/img/fondoFooter.png) no-repeat 0 0 transparent;
    background-position:top;
    margin-top:-25vh;
    z-index:-2;
    grid-area:footer;
  }

  .overlay {
    display:flex;
    justify-content:center;
    position:fixed;
    top:0;
    left:0;
    width:100%;
    height:100%;
    background-color: rgba(0,0,0,0.6);
  }

  @keyframes compartiendo {
    0% { transform:rotate(-30deg); }
    50% { transform:rotate(30deg); }
    100% { transform:rotate(-30deg); }
  }
  .overlay img{
    position:absolute;
    display:block;
    margin-right:auto;
  }
  .overlay img#overlayTitle{
    margin-top:3%;
    margin-right:auto;
  }

  .overlay img#gracias{
    margin-top:80%;
  }
  .overlay img#compartiendo{
    margin-top:80%;
    animation-name:compartiendo;
    animation-duration:2s;
    animation-iteration-count:infinite;
  }

`

const app = choo()

app.use(devtools())

const formView = require('./views/formView.js')

app.use((state, emitter) => {
  function reset () {
    state.sendingStatus = 'init'
    state.consigna = ''
    state.msgLen = 0
    state.msgLenDisplay = 0
    state.msg = ''
    state.msgLimit = 200
    state.canShare = false
    xhr.get('/getConsigna', (err, res) => {
      if (err)console.error(err)
      var obj = JSON.parse(res.body)
      state.consigna = obj['consigna']
      emitter.emit('render')
    })
  }

  // Cargar consigna
  emitter.on('DOMContentLoaded', () => {
    reset()
  })

  emitter.on('newChar', (t) => {
    if (state.msg.length === 0) {
      state.msg = t.trim()
    } else {
      state.msg = t.trimStart()
    }
    console.log(state.msg)
    state.msgLen = state.msg.length
    if (state.msgLen > state.msgLimit) {
      state.msgLenDisplay = state.msgLimit
    } else {
      state.msgLenDisplay = state.msgLen
    }
    emitter.emit('render')
  })

  // Checkeo si ya puede compartir
  setInterval(checkValidInputs, 2000)

  function checkValidInputs () {
    var nombre = document.getElementById('nombre')
    var nameValid = nombre.value.trimStart().length > 0
    var msgValid = state.msgLen > 0
    if (nameValid && msgValid) {
      state.canShare = true
      emitter.emit('render')
    } else {
      state.canShare = false
    }
  }

  // POST
  emitter.on('newFoto', body => {
    state.sendingStatus = 'pending'
    emitter.emit('render')
    console.log(body)
    var postObj = {
      method: 'post',
      body: body,
      uri: '/addUser'
    }
    xhr(postObj, (err, res, body) => {
      if (err)console.error(err)
      console.log(res, body)
      state.sendingStatus = 'sent'
    })

    function checkIfSended () {
      setTimeout(() => {
        if (state.sendingStatus === 'pending') {
          checkIfSended()
        } else {
          setTimeout(() => {
            reset()
            emitter.emit('render')
          }, 3000)
          emitter.emit('render')
        }
      }, 3000)
    }
    checkIfSended()
  })
})

app.route('/', (state, emit) => {
  return html`
  <body class="${style} overflow-hidden w-100-ns">
    <header>
      <img src="assets/img/title.png">
    </header>
    ${formView(state, emit)}
    ${state.sendingStatus !== 'init' ? overlay(state, emit) : ''}
    <footer>
    </footer>
  </body>
  `
})

function overlay (state, emit) {
  return html`
  <div class='overlay'>
    <img id="overlayTitle" src="assets/img/title.png">
    <img id=${showImg().id} src= ${showImg().src}>
    </img>
  </div>
  `
  function showImg () {
    var src = ''
    var id = ''
    if (state.sendingStatus === 'pending') {
      src = 'compartiendo.png'
      id = 'compartiendo'
    } else {
      src = 'gracias.png'
      id = 'gracias'
    }
    return { src: `assets/img/${src}`, id: id }
  }
}

app.mount('body')
