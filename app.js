const http = require('http')
const path = require('path')

const serve = require('serve-static')
const Router = require('routes-router')
const formidable = require('formidable')
const sendJson = require('send-data/json')

const PORT = process.env.NODE_ENV === 'prod' ? process.env.PORT : 8000

const DB = require('./db.js')

const CONSIGNAS = [
  'miércoles, mi día en el campus',
  'jueves, mi día en el campus',
  'viernes, mi día en el campus',
  'sábado, mi día en el campus',
  'domingo, mi día en el campus',
  'lunes, mi día en el campus'
]

var consignaIdx = 0

var app = Router()
var srv = serve(path.join(__dirname, 'public'))

DB((err, db) => {
  if (err) console.error(err)
  console.log('db up')
  setRoutes(db)
  http.createServer(app).listen(PORT, () => console.log(`server up on ${PORT}`))
})

function setRoutes (db) {
  app.addRoute('/getConsigna', (req, res) => {
    return sendJson(req, res, {
      statusCode: 200,
      body: { consigna: CONSIGNAS[consignaIdx], idx: consignaIdx }
    })
  })

  app.addRoute('/setConsigna/:consigna', (req, res, params) => {
    consignaIdx = params.params['consigna'] % CONSIGNAS.length
    console.log('nueva consigna', CONSIGNAS[consignaIdx])
    return sendJson(req, res, {
      statusCode: 200,
      body: { status: 'OK', consigna: CONSIGNAS[consignaIdx] }
    })
  })

  app.addRoute('/getUsrs/:n', (req, res, params) => {
    var nFotos = parseInt(params.params['n'])
    db.getUsers(nFotos, (err, result) => {
      if (err) return sendErr(req, res, err)
      return sendJson(req, res, {
        statusCode: 200,
        body: { status: 'ok', data: result }
      })
    })
  })

  app.addRoute('/addUser', (req, res) => {
    const form = formidable()
    form.parse(req, (err, fields, files) => {
      if (err) return sendErr(req, res, err)

      fields['consigna'] = consignaIdx

      function stripMsg () {
        if (fields['mensaje'].charAt(0) === ' ' ||
        fields['mensaje'].charAt(0) === '/n') {
          fields['mensaje'] = fields['mensaje'].slice(1)
          console.log('hola')
          stripMsg()
        }
        if (fields['mensaje'].charAt(fields['mensaje'].length - 1) === ' ' ||
        fields['mensaje'].charAt(fields['mensaje'].length - 1) === '/n' ||
        fields['mensaje'].charAt(fields['mensaje'].length - 1) === '/r') {
          fields['mensaje'] = fields['mensaje'].slice(1)
          stripMsg()
        }
        fields['mensaje'] = fields['mensaje'].trimStart().trimEnd()
      }

      stripMsg()

      if (fields['nombre'] === '') {
        fields['nombre'] = '...'
      }

      if (fields['mensaje'] === '') {
        fields['mensaje'] = '...'
      }

      if (fields['submit']) {
        delete fields['submit']
      }

      db.addUser(fields, files, (err, fields) => {
        if (err) return sendErr(req, res, err)
        console.log(`guardado: ${JSON.stringify(fields, null, 2)}`)
        return sendJson(req, res, {
          statusCode: 200,
          body: { status: 'ok', data: fields }
        })
      })
    })
  })

  app.addRoute('/img/:imgPath', (req, res, params) => {
    var imgPath = params.params.imgPath
    db.getImg(imgPath, (err, img) => {
      if (err) return sendErr(req, res, err)
      res.writeHead(200, { 'Content-Type': 'image/jpg' })
      res.end(img, 'binary')
    })
  })

  app.addRoute('/purge', (req, res) => {
    db.purgeDb((err) => {
      if (err) return sendErr(req, res, err)
      return sendJson(req, res, {
        statusCode: 200,
        body: { status: 'ok' }
      })
    })
  })

  app.addRoute('/delete/:n', (req, res, params) => {
    var limit = parseInt(params.params.n)
    db.deleteUsers(limit, (err) => {
      if (err) return sendErr(req, res, err)
      return sendJson(req, res, {
        statusCode: 200,
        body: { status: 'ok' }
      })
    })
  })

  app.addRoute('/test/:nombre/:mensaje', (req, res, params) => {
    console.log(params)
    var fields = params.params
    db.testUser(fields, (err) => {
      if (err) return sendErr(req, res, err)
      return sendJson(req, res, {
        statusCode: 200,
        body: { status: 'ok' }
      })
    })
  })

  app.addRoute('*', (req, res) => {
    srv(req, res, () => console.log('serving web'))
  })
}

function sendErr (req, res, err) {
  return sendJson(req, res, {
    statusCode: 200,
    body: { status: err, info: err.message }
  })
}
